package com.example.uploader.Activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.uploader.Model.Medicion;
import com.example.uploader.Model.Paciente;
import com.example.uploader.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class segundaActivity extends AppCompatActivity {

    private Button btnGuardar;
    private Button btnAleatorio;
    private EditText editNombre;
    private EditText editDocumento;
    private EditText editTelefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
        btnGuardar=findViewById(R.id.btnGuardarPaciente);
        btnAleatorio=findViewById(R.id.btnPacienteAleatorio);
        editNombre=findViewById(R.id.editNombre);
        editDocumento=findViewById(R.id.editDocumento);
        editTelefono=findViewById(R.id.editTelefono);

        ActionBar laTool= getSupportActionBar();
        assert laTool != null;
        laTool.setDisplayHomeAsUpEnabled(true);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editDocumento.getText().toString().isEmpty() || editNombre.getText().toString().isEmpty()|| editTelefono.getText().toString().isEmpty()){
                    Toast.makeText(segundaActivity.this, "Complete todo los datos, o genere paciente aleatorio", Toast.LENGTH_SHORT).show();
                }
                else{
                    Paciente paciente = new Paciente(editNombre.getText().toString(),editDocumento.getText().toString(),editTelefono.getText().toString());
                    crearPacienteFirebase(paciente);
                }
            }

        });

        btnAleatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cargarPacienteAleatorio();
            }
        });
    }

    private void cargarPacienteAleatorio() {
        List<Paciente>pacientes = new ArrayList<>();
        pacientes.add(new Paciente("Carlos Martinez","34692512","011-4134-3274"));
        pacientes.add(new Paciente("Roberto Gomez","31067175","011-1554-5469"));
        pacientes.add(new Paciente("Matías García","25477785","011-0523-9473"));
        pacientes.add(new Paciente("Walter Solis","27187303","011-6170-5291"));
        pacientes.add(new Paciente("Lucas Perez","25687166","011-9716-0665"));
        pacientes.add(new Paciente("Martin Sánchez","30577837","011-9841-1027"));
        pacientes.add(new Paciente("Yamila Ramírez","27497025","011-3442-3206"));
        pacientes.add(new Paciente("Silvina Torres","26924841","011-3926-3552"));
        pacientes.add(new Paciente("Marina Flores","21784217","011-7295-3560"));
        pacientes.add(new Paciente("Sonia Rivera","33423573","011-9328-8661"));
        pacientes.add(new Paciente("Cecilia Gómez","30288494","011-3345-3802"));
        pacientes.add(new Paciente("Paula Díaz","20590787","011-8993-7930"));
        pacientes.add(new Paciente("Silvana Reyes","24820814","011-6379-0650"));
        pacientes.add(new Paciente("Vanesa Cruz","27822310","011-7691-3389"));
        pacientes.add(new Paciente("Hector Morales","25200979","011-1850-5847"));
        pacientes.add(new Paciente("Eduardo Ortiz","21484563","011-1185-8692"));
        pacientes.add(new Paciente("Federico Gutierrez","20396125","011-7736-9458"));
        pacientes.add(new Paciente("Fernando Alemano","34730326","011-3691-2020"));
        pacientes.add(new Paciente("Gerardo Crevie","22371691","011-5277-5853"));
        pacientes.add(new Paciente("Pablo Solente","24922902","011-1755-0704"));
        pacientes.add(new Paciente("Alejandro Paz","26504244","011-3021-2777"));
        pacientes.add(new Paciente("Nicolas dalessio","34858570","011-7893-2006"));
        pacientes.add(new Paciente("Nicolas Albertario","27071491","011-1800-6681"));
        pacientes.add(new Paciente("Pedro Ruiz","23573338","011-7013-5708"));
        pacientes.add(new Paciente("Juan Ibarra","25093370","011-5143-4175"));
        pacientes.add(new Paciente("Ariel Brucha","29194767","011-2233-5699"));
        pacientes.add(new Paciente("Patricio Rao","23898477","011-4853-4266"));
        pacientes.add(new Paciente("Luciana Puan","28700297","011-6866-9548"));
        pacientes.add(new Paciente("Maria Diaz","20715528","011-1778-5085"));
        pacientes.add(new Paciente("Susana Perez","20827136","011-6975-8669"));
        pacientes.add(new Paciente("Teo Briera","34807971","011-5673-1552"));
        pacientes.add(new Paciente("Agustin Alfenso","20361362","011-9833-0178"));
        pacientes.add(new Paciente("Matias Pando","21383264","011-3581-8325"));
        pacientes.add(new Paciente("Solana Paz","23341192","011-6438-0048"));
        pacientes.add(new Paciente("Martina Ruch","29403392","011-3287-0024"));

        Random rnd=new Random();
        int numero=rnd.nextInt(34);

        crearPacienteFirebase(pacientes.get(numero));

    }

    private void crearPacienteFirebase(Paciente paciente) {
        DatabaseReference mPaciente= FirebaseDatabase.getInstance().getReference().child("pacientes").child(paciente.getDocumento());
        mPaciente.setValue(paciente);

        DatabaseReference mPacientesGestor= FirebaseDatabase.getInstance().getReference().child("gestores").child("1234").child("pacientes").child(paciente.getDocumento());
        mPacientesGestor.setValue(paciente.getDocumento());
        editDocumento.setText("");
        editNombre.setText("");
        editTelefono.setText("");
        agregarMedicionFirebase(paciente);

        Toast.makeText(this, paciente.getNombreCompleto()+ " fue agregado a la base", Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }
    private void agregarMedicionFirebase(Paciente paciente) {
        Integer randOxi =  randomEntre(60,100);
        Integer randTemp =  randomEntre(350,420);
        Integer randPuls =  randomEntre(60,170);
        Integer randbat = randomEntre(30,95);
        long unixTime = System.currentTimeMillis() / 1000L;
        String uuid= UUID.randomUUID().toString();

        Medicion laMedicion = new Medicion();
        laMedicion.setOxigenacion(String.valueOf(randOxi));
        laMedicion.setPulsaciones(String.valueOf(randPuls));
        laMedicion.setTemperatura(String.valueOf(randTemp));
        laMedicion.setBateria(String.valueOf(randbat));
        laMedicion.setId_medicion(uuid);
        laMedicion.setTimestamp(String.valueOf(unixTime));
        laMedicion.setIdPacienteRelacionado(paciente.getDocumento());
        DatabaseReference mPaciente=FirebaseDatabase.getInstance().getReference().child("mediciones").child(paciente.getDocumento()).child(laMedicion.getTimestamp());
        mPaciente.setValue(laMedicion);
        Toast.makeText(this, "Medicion Agregada", Toast.LENGTH_SHORT).show();
    }
    private static int randomEntre(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

}
