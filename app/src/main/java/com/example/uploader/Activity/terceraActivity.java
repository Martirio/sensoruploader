package com.example.uploader.Activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.uploader.Adapter_Pacientes;
import com.example.uploader.Model.Medicion;
import com.example.uploader.Model.Paciente;
import com.example.uploader.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class terceraActivity extends AppCompatActivity {
    private RecyclerView recyclerPacientes;
    private Adapter_Pacientes adapter;
    private ValueEventListener paciente_p;
    private List<Paciente> listaPacientes;
    private DatabaseReference mDbPacientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tercera);

        ActionBar laTool= getSupportActionBar();
        assert laTool != null;
        laTool.setDisplayHomeAsUpEnabled(true);

        recyclerPacientes= findViewById(R.id.recyclerMediciones);
        recyclerPacientes.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL, false));
        adapter=new Adapter_Pacientes();
        recyclerPacientes.setAdapter(adapter);
        adapter.setContext(this);
        listaPacientes = new ArrayList<>();
        adapter.setListaPacientesOriginales(listaPacientes);
        adapter.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               int posicion = recyclerPacientes.getChildAdapterPosition(view);
                List<Paciente> listaAuditorias = adapter.getListaPacientesOriginales();
                Paciente pacienteClickeado = listaAuditorias.get(posicion);
                agregarMedicionFirebase(pacienteClickeado);
            }
        });
        cargarRecycler();

    }

    private void agregarMedicionFirebase(Paciente paciente) {
        Integer randOxi =  randomEntre(60,100);
        Integer randTemp =  randomEntre(350,420);
        Integer randPuls =  randomEntre(60,170);
        Integer randbat = randomEntre(30,95);
        long unixTime = System.currentTimeMillis() / 1000L;
        String uuid= UUID.randomUUID().toString();

        Medicion laMedicion = new Medicion();
        laMedicion.setOxigenacion(String.valueOf(randOxi));
        laMedicion.setPulsaciones(String.valueOf(randPuls));
        laMedicion.setTemperatura(String.valueOf(randTemp));
        laMedicion.setBateria(String.valueOf(randbat));
        laMedicion.setId_medicion(uuid);
        laMedicion.setTimestamp(String.valueOf(unixTime));
        laMedicion.setIdPacienteRelacionado(paciente.getDocumento());
        DatabaseReference mPaciente=FirebaseDatabase.getInstance().getReference().child("mediciones").child(paciente.getDocumento()).child(laMedicion.getTimestamp());
        mPaciente.setValue(laMedicion);
        Toast.makeText(this, "Medicion Agregada", Toast.LENGTH_SHORT).show();
    }

    private void cargarRecycler() {
        mDbPacientes= FirebaseDatabase.getInstance().getReference().child("pacientes");
        paciente_p  = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listaPacientes.clear();
                for (DataSnapshot nodoPaciente :
                        dataSnapshot.getChildren()) {
                    Paciente pacienteCargar=nodoPaciente.getValue(Paciente.class);
                    listaPacientes.add(pacienteCargar);
                }
                adapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        mDbPacientes.addValueEventListener(paciente_p);
    }

    private static int randomEntre(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }
}
