package com.example.uploader.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.uploader.R;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnNuevoPaciente =findViewById(R.id.btnAgregarPaciente);
        btnNuevoPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                irNuevoPacienteActivity();
            }
        });

        Button btnCargarMedicion= findViewById(R.id.btnAgregarMedicion);
        btnCargarMedicion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                irAVerPacientes();
            }


        });
    }

    private void irNuevoPacienteActivity() {
        Intent intent = new Intent(this, segundaActivity.class);
        startActivity(intent);
    }

    private void irAVerPacientes() {
        Intent intent = new Intent(this, terceraActivity.class);
        startActivity(intent);
    }
}
