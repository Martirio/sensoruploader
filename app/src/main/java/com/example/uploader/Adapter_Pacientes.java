package com.example.uploader;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.example.uploader.Model.Paciente;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by elmar on 18/5/2017.
 */

public class Adapter_Pacientes extends RecyclerView.Adapter implements View.OnClickListener, View.OnLongClickListener {


    private Context context;
    private List<Paciente> listaPacientesOriginales;
    private View.OnClickListener listener;
    private AdapterView.OnLongClickListener listenerLong;






    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaPacientesOriginales(List<Paciente> listaPacientesOriginales) {
        this.listaPacientesOriginales = listaPacientesOriginales;
    }

    public void addListaPacientesOriginales(List<Paciente> listaPacientesOriginales) {
        this.listaPacientesOriginales.addAll(listaPacientesOriginales);
    }


    public List<Paciente> getListaPacientesOriginales() {
        return listaPacientesOriginales;
    }

    //crear vista y viewholder
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;
        viewCelda = layoutInflater.inflate(R.layout.detalle_celda, parent, false);
        viewCelda.setOnClickListener(this);

        return new PacienteViewHolder(viewCelda);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Paciente unPaciente = listaPacientesOriginales.get(position);
        PacienteViewHolder pacienteViewHolder = (PacienteViewHolder) holder;
        assert unPaciente != null;
        pacienteViewHolder.cargarPaciente(unPaciente);

    }

    @Override
    public int getItemCount() {
        return listaPacientesOriginales.size();
    }


    public void onClick(View view) {
        listener.onClick(view);
    }

    @Override
    public boolean onLongClick(View v) {
        listenerLong.onLongClick(v);
        return true;
    }


    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private class PacienteViewHolder extends RecyclerView.ViewHolder {

        private TextView tagNombre;




        //private TextView textViewTitulo;


        public PacienteViewHolder(View itemView) {
            super(itemView);

            tagNombre= itemView.findViewById(R.id.tagNombre);
        }

        public void cargarPaciente(final Paciente unPaciente) {

           tagNombre.setText(unPaciente.getNombreCompleto());

        }


    }
}


// Decodes image and scales it to reduce memory consumption