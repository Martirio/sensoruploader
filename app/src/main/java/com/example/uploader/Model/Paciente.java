package com.example.uploader.Model;

import java.util.List;



public class Paciente  {


    private String documento;

    private String telefono;
    private String nombreCompleto;
    private List<Medicion> listaMediciones;

    public Paciente() {
    }

    public Paciente(String documento) {
        this.documento = documento;
    }

    public Paciente(String nombre, String documento, String telefono) {
        this.documento = documento;
        this.nombreCompleto=nombre;
        this.telefono=telefono;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public List<Medicion> getListaMediciones() {
        return listaMediciones;
    }

    public void setListaMediciones(List<Medicion> listaMediciones) {
        this.listaMediciones = listaMediciones;

    }
}
