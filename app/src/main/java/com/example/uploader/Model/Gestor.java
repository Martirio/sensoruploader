package com.example.uploader.Model;

import java.util.List;


public class Gestor{

    private String id_gestor;

    private List<Paciente> listaPacientes;

    public Gestor() {
    }

    public Gestor(String matri) {
        this.id_gestor=matri;
    }

    public String getId_gestor() {
        return id_gestor;
    }

    public void setId_gestor(String id_gestor) {
        this.id_gestor = id_gestor;
    }

    public List<Paciente> getListaPacientes() {
        return listaPacientes;
    }

    public void setListaPacientes(List<Paciente> listaPacientes) {
        this.listaPacientes = listaPacientes;
    }
}
